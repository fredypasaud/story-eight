$(document).ready(function(){
  var inputawal = "Prince"
  console.log("first run")
  $.ajax({
    method: "GET",
    url: "https://www.googleapis.com/books/v1/volumes?q="+inputawal,
    success: function(result){
      $("table").empty();
      $("table").append(
        '<thead class="thead-dark">' +
          '<tr>' +
            '<th scope="col">Book Picture</th>' +
            '<th scope="col">Book Title</th>' +
            '<th scope="col">Book Author</th>' +
          '</tr>'+
        '</thead>'
      );
      for(i = 0; i < result.items.length ; i++){
        $("table").append(
          "<tbody>" +
            "<tr>" +
              "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
              "<td>" + result.items[i].volumeInfo.title + "</td>" +
              "<td>" + result.items[i].volumeInfo.authors + "</td>" +
            "</tr>" +
          "</tbody>"
        );
      }
    }
  })
  $("#searchBox").keyup(
    function(){
      var input = $(this).val();
      var delay = 3000;
      console.log("run run", input)
      $.ajax({
        method: "GET",
        url: "https://www.googleapis.com/books/v1/volumes?q="+input,
        success: function(result){
          setTimeout(function(){
            $("table").empty();
            $("table").append(
              '<thead class="thead-dark">' +
                '<tr>' +
                  '<th scope="col">Book Picture</th>' +
                  '<th scope="col">Book Title</th>' +
                  '<th scope="col">Book Author</th>' +
                '</tr>'+
              '</thead>'
            );
            for(i = 0; i < result.items.length ; i++){
              $("table").append(
                "<tbody>" +
                  "<tr>" +
                    "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                    "<td>" + result.items[i].volumeInfo.title + "</td>" +
                    "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                  "</tr>" +
                "</tbody>"
              );
            }
          },delay);
        }
      })
    }
  );
})
