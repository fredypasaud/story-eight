from django.test import TestCase, LiveServerTestCase
from django.urls import resolve

from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options


# Create your tests here.

class UnitTest(TestCase):

    def test_view(self):
        response = self.client.get('/')
        target = resolve('/')
        self.assertTemplateUsed('index.html')
        self.assertContains(response, 'Search your book here!', html=True)
        self.assertTrue(target.func, index)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        # firefox
        super(FunctionalTest, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertInHTML('Search your book here!', self.browser.page_source)
        search = self.browser.find_element_by_id('searchBox')
        search.send_keys("Harry Potter")
