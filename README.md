[![pipeline status](https://gitlab.com/fredypasaud/story-eight/badges/master/pipeline.svg)](https://gitlab.com/fredypasaud/story-eight/commits/master)

[![coverage report](https://gitlab.com/fredypasaud/story-eight/badges/master/coverage.svg)](https://gitlab.com/fredypasaud/story-eight/commits/master)

## Author
Fredy Pasaud Marolan Silitonga

## Heroku Link
https://story-eight.herokuapp.com/
